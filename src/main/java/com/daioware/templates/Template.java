package com.daioware.templates;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;

import com.daioware.commons.string.Regex;
import com.daioware.file.FileUtil;

import java.io.File;
import java.io.IOException;

public class Template {
	public static final String REGEX_EXP="\\$[\\w.]+";
	public static final Regex DEFAULT_REGEX=new Regex(REGEX_EXP);
	private String text;
	private Regex regex;
	private Map<String,String> map;
	
	protected Template(int params) {
		map=new HashMap<>(params);
	}
	protected Template() {
		map=new HashMap<>();
	}
	public Template(File file,Charset charset) throws IOException {
		this(FileUtil.getContents(file, charset));
	}
	public Template(File file,Charset charset,Regex regex) throws IOException {
		this(FileUtil.getContents(file, charset),regex);
	}
	public Template(String text) {
		this(text,DEFAULT_REGEX);
	}
	public Template(String text,int params) {
		this(text,DEFAULT_REGEX,params);
	}
	public Template(String text, Regex regex) {
		this();
		setText(text);
		setRegex(regex);
	}
	public Template(String text, Regex regex,int params) {
		this(params);
		setText(text);
		setRegex(regex);
	}
	public void put(String key,Object value) {
		map.put(key, value.toString());
	}
	public void put(Map<String,Object> map) {
		for(Entry<String,Object> entry:map.entrySet()) {
			put(entry.getKey(),entry.getValue());
		}
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Regex getRegex() {
		return regex;
	}
	public void setRegex(Regex regex) {
		this.regex = regex;
	}
	
	protected int findFirstLetter(String str) {
		for(int i=0,j=str.length();i<j;i++) {
			if(Character.isLetter(str.charAt(i))) {
				return i;
			}
		}
		return -1;
	}
	protected int findLastCharacterFromVariable(int beginning,String str) {
		char c;
		int i,j;
		for(i=beginning,j=str.length();i<j;i++) {
			c=str.charAt(i);
			if(c!='.' && !Character.isLetter(c) &&  c!='_' && !Character.isDigit(c)) {
				return i;
			}
		}
		return j;
	}
	protected String getRealVariable(String variable) {
		int begPos,endPos;
		begPos=findFirstLetter(variable);
		endPos=findLastCharacterFromVariable(begPos+1,variable);
		return variable.substring(begPos,endPos);
	}
	public String giveMeValueOfVariableNotFound(String variable) {
		return variable;
		//return "";
	}
	public String getTextProcessed() {
		Matcher matcher=regex.getPattern().matcher(getText());
		String variable;		
		String value;
		StringBuffer textProcessed=new StringBuffer();
		while(matcher.find()) {
			value=map.get(variable=getRealVariable(matcher.group()));
			if(value!=null) {
				matcher.appendReplacement(textProcessed, value);
			}
			else {
				matcher.appendReplacement(textProcessed,giveMeValueOfVariableNotFound(variable));
			}
		}
		matcher.appendTail(textProcessed);
		return textProcessed.toString();
	}
	public String toString() {
		return getTextProcessed();
	}
	public static void main(String[] args) {
		Template template=new Template("todos sabemos que $var1 es $var2 je je x:$var3.a y:$var3.a.c :v");
		template.put("var1",1);
		template.put("var2",2);
		template.put("var3.a",3);
		template.put("var3.a.c",5);
		String textProcessed=template.getTextProcessed();
		Regex regex=new Regex(REGEX_EXP);
		System.out.println(regex.matches("$hola"));
		System.out.println(regex.matches("$b.2"));
		System.out.println(textProcessed);	
	}
	
	
}
