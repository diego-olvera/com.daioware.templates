package com.daioware.templates;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Date;

import com.daioware.commons.string.Regex;

public class SQLTemplate extends Template {
	
	private static final Regex regex=new Regex("\\?[0-9]+");
	
	public SQLTemplate(File file, Charset charset) throws IOException {
		super(file, charset, regex);
	}

	public SQLTemplate(String text) {
		super(text, regex);
	}
	
	public void setText(String text) {
		StringBuilder info=new StringBuilder();
		int questionMarkIndex,i,j;
		char c;
		for(i=questionMarkIndex=0,j=text.length();i<j;i++) {
			c=text.charAt(i);
			info.append(c);
			if(c=='?') {
				info.append(questionMarkIndex++);
			}
		}
		super.setText(info.toString());
	}
	public static String escapeString(String s) {
		return "'"+s.replaceAll("'","''")+"'";
	}
	public void setString(int index,String s) {
		put(index,escapeString(s));
	}
	public void setString(String id,String s) {
		put(id,escapeString(s));
	}
	public void setBigDecimal(int index,BigDecimal b) {
		put(index,b);
	}
	public void setBigDecimal(String index,BigDecimal b) {
		put(index,b);
	}
	public void setBigInteger(int index,BigInteger b) {
		put(index,b);
	}
	public void setBigInteger(String index,BigInteger b) {
		put(index,b);
	}
	public void setDouble(int index,double b) {
		put(index,b);
	}
	public void setDouble(String index,double b) {
		put(index,b);
	}
	public void setFloat(int index,float b) {
		put(index,b);
	}
	public void setFloat(String index,float b) {
		put(index,b);
	}
	public void setLong(int index,long l) {
		put(index,l);
	}
	public void setLong(String index,long l) {
		put(index,l);
	}
	public void setInt(int index,int l) {
		put(index,l);
	}
	public void setInt(String index,int l) {
		put(index,l);
	}
	public void setShort(int index,short l) {
		put(index,l);
	}
	public void setShort(String index,short l) {
		put(index,l);
	}
	public void setChar(int index,char c) {
		setString(index,String.valueOf(c));
	}
	public void setChar(String index,char c) {
		setString(index,String.valueOf(c));
	}
	public void setBoolean(int index,boolean c) {
		put(index,c?"true":"false");
	}
	public void setBoolean(String index,boolean c) {
		put(index,c?"true":"false");
	}
	public void setDate(int index,Date date) {
		put(index,date.getTime());
	}
	public void setDate(String index,Date date) {
		put(index,date.getTime());
	}
	public void setTimestamp(int index,Timestamp date) {
		put(index,date.getTime());
	}
	public void setTimestamp(String index,Timestamp date) {
		put(index,date.getTime());
	}
	public void put(int index,Object obj) {
		put("?"+(index-1),obj);
	}

	public void setObject(int index,Object object) {
		if(object instanceof String) {
			setString(index, (String)object);
		}
		else if(object instanceof Date) {
			setDate(index,(Date)object);
		}
		else{
			setBigDecimal(index,new BigDecimal(object.toString()));
		}
	}
	protected String getRealVariable(String variable) {
		return variable;
	}
	public static void main(String[] args) {
		String sql="select * from alergia where nombre=? and dinero=?";
		SQLTemplate template=new SQLTemplate(sql);
		template.setString(1,"' drop table alergia;");
		template.setDouble(2,444444444444444444.436666666666666);
		System.out.println(template.getTextProcessed());	
	}

}
